## Up and Running
Ruby version `2.5.0` must be installed. I used `2.6.0`.
If using RVM, `rvm use 2.6.0`

In the `megaphone` directory:
```bash
$ bundle install
```

Create a megaphone user in Postgres:
```sql
create role megaphone with createdb login password 'your_password';
```

Setup the databases:
```bash
$ rake db:setup
```

To run the Rails server:
```bash
$ rails s
```

Navigate to `localhost:3000` in your browser, and you should see the webpage.

Once you register a user, you should be able to upload and delete CSVs.

## Running Tests
From inside the `megaphone` folder:
```bash
rails test
```

## Next Steps
1) Add specs. At least a few unit tests. Feature/End-to-End tests would be nice to have.
1) Error handling - specifically around mal-formed CSVs
1) Ensure prices are to two decimals
1) Add styling to UI
1) Row-level deletion of invoices
1) Sorting of invoices
1) Paging of invoices
1) Consider user-level control of invoices
1) Normalization of data - Products, Merchants, Customers, etc.
1) Address issues that arise from large CSVs (background processing, file-size limit, separate service?)
1) Email verification
1) Remove boilerplate code
