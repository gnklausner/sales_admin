Rails.application.routes.draw do
  root to: 'invoices#index'

  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :invoices, only: [:index, :import, :delete] do
    get 'index', on: :collection
    post 'import', on: :collection
    delete 'destroy', on: :collection
  end
end
