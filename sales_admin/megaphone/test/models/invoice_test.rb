require 'test_helper'

class InvoiceTest < ActiveSupport::TestCase
  test 'total is accurate' do
    invoice = Invoice.create(
      customer_name: 'Jack Burton',
      item_description: 'Premium Cowboy Boots',
      item_price: 14999,
      quantity: '4',
      merchant_name: 'Carpenter Outfitters',
      merchant_address: '99 Factory Drive'
    )

    assert invoice.total == 599.96
  end

  test 'import creates appropriate records' do
    starting_record_count = Invoice.all.size

    fixture_path = file_fixture('salesdata.csv').to_path
    file = File.open(fixture_path)
    line_count = File.foreach(fixture_path).reduce(0) { |c, line| c + 1 }
    record_count = line_count - 1 # remove count for header line

    Invoice.import(file)
    assert Invoice.all.size == (record_count + starting_record_count)
  end
end
