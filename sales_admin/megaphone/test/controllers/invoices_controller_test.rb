require 'test_helper'

class InvoicesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user ||= User.create(email: 'test@tester.com', password: 'correcthorsebatterystaple')
    sign_in @user
  end

  test "should get index" do
    get invoices_url
    assert_response :success
  end

  test "should redirect get index when signed out" do
    sign_out @user
    get invoices_url
    assert_response :redirect
  end

end
