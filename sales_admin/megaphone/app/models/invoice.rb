require 'csv'

class Invoice < ApplicationRecord
  def self.import(file)
    invoice_array = [].tap do |arr|
      CSV.foreach(file.path,
                headers: true,
                header_converters: ->(name) { name.downcase.sub(' ', '_') }) do |invoice|
        invoice['item_price'] = invoice['item_price'].to_f * 100
        arr.push invoice.to_h
      end
    end
    Invoice.insert_all! invoice_array
  end

  def item_price
    self[:item_price].to_f / 100.0
  end

  def total
    item_price * quantity
  end
end
