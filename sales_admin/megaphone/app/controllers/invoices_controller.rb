class InvoicesController < ApplicationController
  def index
    @invoices = Invoice.all
    @sum = @invoices.reduce(0) { |sum, invoice| sum + invoice.total }
  end

  def import
    Invoice.import(params[:file])
    redirect_to root_url
  rescue StandardError => e
    Rails.logger.error e
    redirect_to root_url, notice: 'There was an issue loading your CSV'
  end

  def destroy
    Invoice.destroy_all
    redirect_to root_url
  end
end
