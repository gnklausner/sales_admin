class AddNotNullConstraintsOnInvoices < ActiveRecord::Migration[6.0]
  def change
    change_column_null :invoices, :customer_name, false
    change_column_null :invoices, :item_description, false
    change_column_null :invoices, :item_price, false
    change_column_null :invoices, :quantity, false
    change_column_null :invoices, :merchant_name, false
    change_column_null :invoices, :merchant_address, false
  end
end
