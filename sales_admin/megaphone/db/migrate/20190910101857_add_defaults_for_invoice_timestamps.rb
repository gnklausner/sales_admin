class AddDefaultsForInvoiceTimestamps < ActiveRecord::Migration[6.0]
  def change
    change_column_default :invoices, :created_at, -> { 'CURRENT_TIMESTAMP' }
    change_column_default :invoices, :updated_at, -> { 'CURRENT_TIMESTAMP' }
  end
end
